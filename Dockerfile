FROM ruby:2.5.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /autocomp
WORKDIR /autocomp

ADD Gemfile /autocomp/Gemfile
ADD Gemfile.lock /autocomp/Gemfile.lock

RUN bundle install

ADD . /autocomp
